#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

int checkIfAnagram(char[],char[]);
void simpleList();
int sumElements(int[],int);
void stretch(char[]);
int orderCheck(int[], int);
void displayList(int[],int);
int returnNElement(int[],int);
int *removeNElement(int*, int, int);
int *addElement(int*, int, int);
int compareArrays(int[], int[], int);

int main(){
    int array[] = {5,4,2,3};
    int array2[] = {5,4,3};
    int array1[] = {5,2,3};
    int array3[] = {5,4,2};
    int array22[] = {5,4,2,3,2};
    int array11[] = {5,4,2,3,1};
    int array33[] = {5,4,2,3,3};
    int arrayAsc[] = {1,2,3,4,5};

    //Assert check if anagram
    assert(checkIfAnagram("cat", "tac") == 1);
    assert(checkIfAnagram("cat", "tas") != 1);
    assert(checkIfAnagram("cat", "tassda") != 1);
    assert(checkIfAnagram("catdasd", "tas") != 1);
    
    //Assert return N Element
    assert(returnNElement(array,2)==2);
    assert(returnNElement(array,1)==4);
    assert(returnNElement(array,3)==3);

    //Assert return N Element
    assert(compareArrays(removeNElement(array,2,4),array2,3)==1);
    assert(compareArrays(removeNElement(array,1,4),array1,3)==1);
    assert(compareArrays(removeNElement(array,3,4),array3,3)==1);
    
    //Assert add Element
    assert(compareArrays(addElement(array,2,5),array2,3)==1);
    assert(compareArrays(addElement(array,1,5),array1,3)==1);
    assert(compareArrays(addElement(array,3,5),array3,3)==1);

    //Assert sum
    assert(sumElements(array22,5)==7);
    assert(sumElements(array11,5)==9);
    assert(sumElements(array,4)==7);

    //Assert Order Check
    assert(orderCheck(array22,5) == 0);
    assert(orderCheck(array2,3) == -1);
    assert(orderCheck(arrayAsc,5) == 1);

}

int compareArrays(int array1[], int array2[], int size){
    for(int i =0; i < size; i++){
        if(array1[i] != array2[i]){
            return 0;
        }
        return 1;
    }
}

int checkIfAnagram(char word1[],char word2[]){
    if(strlen(word1) != strlen(word2)){
        return 0;   
    }
    int first[26] = {0}, second[26] = {0}, c=0;

    while(word1[c] != '\0'){
        int lower = tolower(word1[c]);
        first[lower - 'a']++;
        c++;
    }

    c = 0;

    while(word2[c] != '\0'){
        int lower = tolower(word2[c]);
        second[lower - 'a']++;
        c++;
    }

    for(c = 0; c < 26; c++){
        if(first[c] != second[c]){
            return 0;
        }
    }
    return 1;
    
}

void displayList(int numbers[],int size){
    for(int i = 0; i < size; i++){
        printf("%d ",numbers[i]);
    }
}

int returnNElement(int numbers[], int nElement){
    return numbers[nElement];
}

int *addElement(int *numbers, int element, int size){ 
    int *nums = malloc(size*sizeof(int));
    
    for(int i = 0; i < size; i++){
        nums[i] = numbers[i];
    }
    nums[size]=element;
    
    
    return nums;
}

int *removeNElement(int *numbers, int nElement, int size){ 
    int *nums = malloc((size-1)*sizeof(int));
    
    for(int i = 0; i < nElement; i++){
        nums[i] = numbers[i];
    }
    for(int i = nElement+1; i < size; i++){
        nums[i-1] = numbers[i];
    }
    
    return nums;
}

int sumElements(int numbers[],int size){
    int max, min, sum = 0 ;
    if(size <= 2){
        return 0;
    }
    min = numbers[0];
    max = numbers[0];
    for(int i = 1; i < size; i++){
        if(numbers[i] < min){
            min = numbers[i];
        }else if(numbers[i] > max){
            max = numbers[i];
        }
    }
    for(int i = 0; i < size;i++){
        if(numbers[i] != min && numbers[i] != max){
            sum += numbers[i];
        }
    }
    return sum;
}

void stretch(char word[]){
    for(int i = 0; i < strlen(word); i++){
        if(word[i] == '\0'){
            printf("%c",'\0');
        }else{
            for(int j = 0; j < i+1;j++){
                if(j == 0){
                    printf("%c",toupper(word[i]));    
                }else{
                    printf("%c",word[i]);
                }
            }
            printf("-");
        }
    }
}

int orderCheck(int numbers[], int size){
    int descending, temp;
    descending = temp = (numbers[0] > numbers[1])?-1:1;
    
    for(int i = 1; i < size-1; i++){
        temp = (numbers[i] > numbers[i+1])?-1:1;
        if(temp != descending){
            return 0;
        }
    }
    return descending;
}